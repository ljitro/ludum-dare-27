package
{
  import loom.Application;
  import loom.gameframework.TimeManager;
  import loom.animation.LoomTween;
  import loom.animation.LoomEaseType;
  
  import loom2d.display.StageScaleMode;
  import loom2d.display.Image;
  import loom2d.display.Sprite;
  import loom2d.display.DisplayObject;
  import loom2d.display.MovieClip;
  import loom2d.Loom2D;

  import loom2d.ui.TextureAtlasManager;
  import loom2d.ui.TextureAtlasSprite;
  import loom2d.textures.Texture;
  import loom2d.textures.TextureAtlas;
  import loom2d.ui.SimpleLabel;
  import loom2d.events.KeyboardEvent;
  import loom2d.animation.Tween;
  import loom2d.animation.Transitions;

  //import loom.gameframework.LoomGroup;

  import loom.platform.LoomKey;
  
  import cocosdenshion.SimpleAudioEngine;

  public class FleethingReality extends Application
  {

    const wallWidth:Number = 135;
    var debug = false;
    var theTimer = new TimeManager();
    var flagReset:Boolean = false;
    var canControl:Boolean = false;
    var channeling:Boolean = false;
    var lockChannel:Boolean = false;
    var demonOut:Boolean = false;
    var moved:Boolean = false;
    var doubleMoved:Boolean = false;
    var chanelSoundID:Number;
    //var bg:Image;
    //var sprite:Image;
    var dx:Number = 0;
    var dy:Number = 0;
    var speed:Number = 1;

    var step:Number = 0; // What phase we are up to.
    var tween:Tween;

    var grid:Vector.<Vector.<int>>;

    //public var grid:Array = new Array();

    public var stepChecker:StepChecker;

    public var animationSheet:TextureAtlas;

    public var playerLayer:Sprite;
    public var wallLayer:Sprite;
    public var doodadLayer:Sprite;
    public var frontLayer:Sprite;

    override public function run():void
    {
      // Comment out this line to turn off automatic scaling.
      stage.scaleMode = StageScaleMode.LETTERBOX;

      if (debug) 
        speed = 3;

      wallLayer = new Sprite();
      stage.addChild(wallLayer);
      group.registerManager(wallLayer, null, "wallLayer");

      doodadLayer = new Sprite();
      stage.addChild(doodadLayer);
      group.registerManager(doodadLayer, null, "doodadLayer");

      playerLayer = new Sprite();
      stage.addChild(playerLayer);
      group.registerManager(playerLayer, null, "playerLayer");
      
      frontLayer = new Sprite();
      stage.addChild(frontLayer);
      group.registerManager(frontLayer, null, "frontLayer");

      TextureAtlasManager.register("sprites", "assets/sprites.xml");
      TextureAtlasManager.register("backgrounds", "assets/backgrounds.xml");

      //animationSheet = new TextureAtlas("assets/animations.png","assets/animations.xml");

      makeTextureAtlasSprite("bg","backgrounds","bg",stage.stageWidth/2,stage.stageHeight/2,1,wallLayer,true);
      makeTextureAtlasSprite("bg2","backgrounds","bg2",stage.stageWidth/2,stage.stageHeight/2,0,wallLayer,true);
      makeTextureAtlasSprite("bg3","backgrounds","bg3",stage.stageWidth/2,stage.stageHeight/2,0,wallLayer,true);
      makeTextureAtlasSprite("newPortal","backgrounds","newPortal",stage.stageWidth/2-89,12,0,wallLayer,false);
      makeTextureAtlasSprite("demonCrack","backgrounds","demonCrack",86,425,0,wallLayer,true);

      makeTextureAtlasSprite("windowLight","sprites","windowLight",(stage.stageWidth/2-7),(98/2)+20,0,wallLayer,true);
      makeAni("portal",stage.stageWidth/2-7,(102/2)+20,wallLayer,12,true);
      makeTextureAtlasSprite("windowFrame","sprites","windowFrame",(stage.stageWidth/2-7),(102/2)+20,0,wallLayer,true);
      makeTextureAtlasSprite("shroom","sprites","normalShroom",stage.stageWidth-155,stage.stageHeight/2,0,doodadLayer,false);
      makeTextureAtlasSprite("blueShroom","sprites","blueShroom",301,389,0,doodadLayer,false);
      makeTextureAtlasSprite("crack","sprites","crack",120,42,0,wallLayer,false);
      makeTextureAtlasSprite("meter","sprites","meter",stage.stageWidth-62,stage.stageHeight-103,1,wallLayer,false);
      makeTextureAtlasSprite("level","sprites","level",stage.stageWidth-50,stage.stageHeight-100,1,wallLayer,false);

      makeAni("shadow",90,400,frontLayer,2,true);
      makeAni("death",400,400,frontLayer,4,true);
      frontLayer.getChildByName("death").scale = 0.75;
      //makeAni("suck",stage.stageWidth/2,100,frontLayer,10,false);


      makeAni("man",stage.stageWidth / 2,stage.stageHeight / 2 + 50,playerLayer,8,true);
      getPlayer().scale = 0.75;
      getPlayer().pause();
      getPlayer().setFrameSound( 2, "assets/sfx/shuffle.mp3" );

      makeTextureAtlasSprite("timeDot","sprites","timeDot",stage.stageWidth-20,stage.stageHeight-20,1,wallLayer,true);

      if(debug)
        makeTextureAtlasSprite("dot","sprites","dot",getPlayer().x,getPlayer().y,1,playerLayer,true);

      var bar = new Image(Texture.fromAsset("assets/bar.png"));
      //bar.x = stage.stageWidth / 2;
      bar.name = "bar";
      bar.alpha = 0;
      bar.y = stage.stageHeight-bar.height;
      frontLayer.addChild(bar);

      bar = new Image(Texture.fromAsset("assets/bar2.png"));
      //bar.x = stage.stageWidth / 2;
      bar.name = "bar2";
      bar.alpha = 0;
      bar.y = stage.stageHeight-bar.height;
      frontLayer.addChild(bar);

      var victory = new Image(Texture.fromAsset("assets/victory.png"));
      //victory.x = stage.stageWidth / 2;
      victory.name = "victory";
      victory.alpha = 0;
      frontLayer.addChild(victory);

      // Keyboard
      this.stage.addEventListener(KeyboardEvent.KEY_DOWN,handleKeyDown); 
      this.stage.addEventListener(KeyboardEvent.KEY_UP,handleKeyUp); 
      //stepChecker = new StepChecker(stage.stageHeight,stage.stageWidth);
      intro();
    }

    override public function onTick():void
    {
      var sprite = getPlayer();
      moved = false;
      if(canControl && (sprite.x + speed*dx) < (stage.stageWidth- wallWidth-sprite.width/2) && (sprite.x + speed*dx) > wallWidth+sprite.width/2 && !channeling)
      {
        sprite.x += speed*dx;
        moved = true;
      }
      if(canControl && (sprite.y - speed*dy) < (stage.stageHeight) && (sprite.y - speed*dy) > wallWidth-sprite.height/2+20 && !channeling)
      { 
        moved = true;
        sprite.y -= speed*dy;
      }
      if(moved && (dx!=0||dy!=0))
      {
        getPlayer().play();
        if(dx!=0&&dy!=0)
          getPlayer().fps = 12;
        else
          getPlayer().fps = 8;
      }
      else
        getPlayer().pause();

      if(demonOut && !flagReset)
      {
        //checkCollide();
        moveDemon(1.5);
      }

    }

    public function moveDemon(_sped:Number):void
    {
      var p = getPlayer();
      var d = frontLayer.getChildByName("shadow");
      var distx=p.x-d.x;
      var disty=p.y-d.y;
      var dtotal=Math.sqrt((distx*distx)+(disty*disty));
      if (dtotal<10)
      {
        var death = frontLayer.getChildByName("death") as MovieClip;
        death.stop();
        death.play();
        death.x = p.x;
        death.y = p.y;
        death.alpha = 1;
        LoomTween.to(death, 1,{"alpha":0});
        d.x = -1000;
        d.y = -1000;
        p.x = -1000;
        p.y = -1000;
        flagReset = true;
        canControl = false;

        //inTenSeconds();
        //Console.print("help! " + dtotal);
      }
      if(!channeling)
      {
        d.x += _sped * (distx/dtotal);
        d.y += _sped * (disty/dtotal);
      }
      else
      {
        if((d.x - 1.5*_sped * (distx/dtotal)) < (stage.stageWidth- wallWidth) && (d.x - 1.5*_sped * (distx/dtotal)) > wallWidth)
          d.x -= 1.5*_sped * (distx/dtotal);
        if((d.y + 1.5*_sped * (disty/dtotal)) < (stage.stageHeight) && (d.y + 1.5*_sped * (disty/dtotal)) > wallWidth-d.height/2+20)
          d.y -= 1.5*_sped * (disty/dtotal);
      }
    }

    public function makeTextureAtlasSprite(_name:String,_atlasName:String,_textureName:String,_x:Number,_y:Number,_alpha:Number,_layer:Sprite,_center:Boolean):void
    {
      var tempTexture:TextureAtlasSprite = new TextureAtlasSprite();
      tempTexture.name = _name;
      tempTexture.atlasName = _atlasName;
      tempTexture.textureName = _textureName;
      if(_center)
        tempTexture.center();
      //tempTexture.scale = _scale;
      tempTexture.x = _x;
      tempTexture.y = _y;
      tempTexture.alpha = _alpha;
      _layer.addChild(tempTexture);
    } 

    public function makeAni(_string:String,_x:Number,_y:Number,_layer:Sprite,_refresh:Number,_alpha:Boolean):void
    {
      var frames = new Vector.<Texture>();
      frames.pushSingle(TextureAtlasManager.getTexture("sprites",_string+"1"));
      frames.pushSingle(TextureAtlasManager.getTexture("sprites",_string+"2"));
      frames.pushSingle(TextureAtlasManager.getTexture("sprites",_string+"3"));
      var clip = new MovieClip(frames, _refresh);
      Loom2D.juggler.add(clip);
      clip.name = _string;
      if(_alpha)
        clip.alpha=0;
      clip.play();
      clip.center();
      clip.x = _x;
      clip.y = _y;
      _layer.addChild(clip);

    }

    public function handleKeyDown(_event:KeyboardEvent):void
    {
      var _key = _event.keyCode;
      if(_key == LoomKey.UP_ARROW || _key == LoomKey.W)
        dy += 1;
      if(_key == LoomKey.DOWN_ARROW || _key == LoomKey.S)
        dy-=1;
      if(_key == LoomKey.RIGHT_ARROW || _key == LoomKey.D)
        dx+=1;
      if(_key == LoomKey.LEFT_ARROW || _key == LoomKey.A)
        dx-=1;
      if(_key == LoomKey.SPACEBAR )
      {
        if(canControl && !channeling && !lockChannel)
          fleetReality(0.75);
      }
      if(_key == LoomKey.Q && debug)
      {
        playerLayer.getChildByName("dot").x = getPlayer().x;
        playerLayer.getChildByName("dot").y = getPlayer().y;
        Console.print("x: "+getPlayer().x+ " y: " + getPlayer().y);
      }
      if(_key == LoomKey.E && debug)
      {
        step++;
      }
      if(_key == LoomKey.R && debug)
      {
        stepUp();
      }
    }
    public function handleKeyUp(_event:KeyboardEvent):void
    {
      var _key = _event.keyCode;
      if(_key == LoomKey.UP_ARROW || _key == LoomKey.W)
        dy -=1;
      if(_key == LoomKey.DOWN_ARROW || _key == LoomKey.S)
        dy+=1;
      if(_key == LoomKey.RIGHT_ARROW || _key == LoomKey.D)
        dx-=1;
      if(_key == LoomKey.LEFT_ARROW || _key == LoomKey.A)
        dx+=1;
      if(_key == LoomKey.SPACEBAR)
      {
        //if(canControl)
          //fleetReality(1);
      }
    }

    public function inTenSeconds():void
    {
      if(flagReset)
      {
        getPlayer().x = 175;
        getPlayer().y=142;
        frontLayer.getChildByName("shadow").x = 70;
        frontLayer.getChildByName("shadow").y = 400;
        flagReset = false;
        canControl = true;
      }
      var bg = wallLayer;
      SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/tenSecShift2.mp3");
      //frontLayer.getChildByName("bar").alpha= 0;
      LoomTween.to(bg, 0.1,{"scale":0.99,"x":bg.x+10});
      LoomTween.to(bg, 0.1,{"delay":0.1, "x":bg.x-10 /*"ease":LoomEaseType.EASE_IN_BOUNCE*/});
      LoomTween.to(bg, 0.1,{"delay":0.2,"scale":1, "x":bg.x /*"ease":LoomEaseType.EASE_IN_BOUNCE*/});
      theTimer.schedule(10000,this,inTenSeconds);
      //theTimer.schedule(250,this,checkTrigger);
      checkTrigger();
      //frontLayer.getChildByName("bar").alpha = 1;
    }

    public function checkTrigger():void
    {
      if (channeling)
      {
        stepUp();
      }
    }

    public function fleetReality(_amount:float):void
    {
      var sprite = getPlayer();
      
      if(_amount!=1)
      {
        chanelSoundID = SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/channel3.mp3",true);
        channeling = true;
        theTimer.schedule(2000,this,toggleChannel);
        LoomTween.to(sprite, 0.1,{"alpha":_amount});
        LoomTween.to(wallLayer.getChildByName("level"), 2,{"y":stage.stageHeight});
        LoomTween.to(wallLayer.getChildByName("level"), 3,{"delay":2, "y":stage.stageHeight-100});
        lockChannel = true;
        theTimer.schedule(5000,this,toggleLockChannel);

      }
      else
      {
        //channeling = false;
        //SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/channeldown3.mp3", false);
        //SimpleAudioEngine.sharedEngine().pauseEffect(chanelSoundID);
      }
    }

    public function toggleChannel():void
    {
      {
        channeling = false;
        SimpleAudioEngine.sharedEngine().pauseEffect(chanelSoundID);
        LoomTween.to(getPlayer(), 0.1,{"alpha":1});
      }
    }

    public function toggleLockChannel():void
    {
      lockChannel = false;
    }

    public function stepUp():void
    {
      var sprite = getPlayer();
      var xPoint = stage.stageWidth/2;
      var yPoint = stage.stageHeight/2;

      if(step == 0)
      { 
        LoomTween.to(wallLayer.getChildByName("windowFrame"),3,{"alpha":1});
        step++;
      }
      else if(step==1)
      {
        if(sprite.x < xPoint+sprite.width/2 && sprite.x > xPoint-sprite.width/2 && sprite.y<190)
        {
          LoomTween.to(wallLayer.getChildByName("windowLight"),3,{"alpha":1});
          LoomTween.to(doodadLayer.getChildByName("shroom"),3,{"alpha":1});
          step++;
        }
      }
      else if(step==2)
      {
        xPoint = stage.stageWidth-155;
        if(sprite.x < xPoint+sprite.width && sprite.x > xPoint-sprite.width/2 &&
           sprite.y < yPoint+sprite.height/2 && sprite.y > yPoint - sprite.height/2)
        {
          LoomTween.to(doodadLayer.getChildByName("shroom"),3,{"alpha":0});
          LoomTween.to(doodadLayer.getChildByName("blueShroom"),3,{"alpha":1});
          step++;

        }
      }
      else if(step==3)
      { 
        xPoint = 301;
        yPoint=389;
        if(sprite.x < xPoint+sprite.width && sprite.x > xPoint-sprite.width/2 
           && sprite.y < yPoint+sprite.height/2 && sprite.y > yPoint - sprite.height/2)
        {
          LoomTween.to(wallLayer.getChildByName("bg2"),3,{"alpha":1});
          LoomTween.to(wallLayer.getChildByName("bg"),3,{"delay":3, "alpha":0});
          LoomTween.to(doodadLayer.getChildByName("blueShroom"),3,{"alpha":0});
          LoomTween.to(wallLayer.getChildByName("windowLight"),3,{"alpha":0});
          LoomTween.to(wallLayer.getChildByName("portal"),3,{"alpha":1});

          step++;
        }
      }
      else if(step==4)
      { 
        xPoint = 301;
        yPoint=389;
        if(sprite.x < 764 && sprite.x > 614 && sprite.y < 242)
        {
          LoomTween.to(wallLayer.getChildByName("bg3"),1,{"alpha":1});
          LoomTween.to(frontLayer.getChildByName("bar2"),1,{"alpha":1});
          LoomTween.to(wallLayer.getChildByName("bg2"),1,{"delay":3, "alpha":0});

          step++;
        }
      }

      else if(step==5)
      { 
        xPoint = 175;
        yPoint=142;
        if(sprite.x < xPoint+sprite.width && sprite.x > xPoint-sprite.width/2 
           && sprite.y < yPoint+sprite.height/2 && sprite.y > yPoint - sprite.height/2)
        {
          LoomTween.to(frontLayer.getChildByName("shadow"),1,{ "alpha":1});
          LoomTween.to(wallLayer.getChildByName("demonCrack"),1,{ "alpha":1});
          demonOut = true;
          step++;
        }
      }
      else if(step==6)
      {
        var demon = frontLayer.getChildByName("shadow");
        if(demon.x < xPoint+demon.width && demon.x > xPoint-demon.width && demon.y<250)
        {
          
          LoomTween.to(wallLayer.getChildByName("newPortal"),1,{ "alpha":1});
          demonOut = false;
          LoomTween.to(wallLayer.getChildByName("windowFrame"),0,{"alpha":0});
          LoomTween.to(demon,3,{"scale":0.5, "y":demon.y-20, "alpha":0});
          step++;
        }
      }
      else if(step==7)
      {
        if(sprite.x < xPoint+sprite.width/2 && sprite.x > xPoint-sprite.width/2 && sprite.y<190)
        {
          LoomTween.to(frontLayer.getChildByName("victory"),1,{ "alpha":1});
          canControl = false;
          SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/channel3.mp3",true);

          //step++;
        }
      }
    }

    public function getPlayer():MovieClip
    {
      return playerLayer.getChildByName("man") as MovieClip;

    }


    public function intro():void
    {

      if(!debug)
      {
        wallLayer.alpha = 0;

        SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/intro2.mp3");
        LoomTween.to(getPlayer(),3,{"alpha":1});
        LoomTween.to(wallLayer,3,{"delay":4, "alpha":1});
        LoomTween.to(frontLayer.getChildByName("bar"),3,{"delay":5 , "alpha":1});
        theTimer.schedule(10000,this,inTenSeconds);
        ticker();
        theTimer.schedule(6000,this,switchPlay);
        

      }
      else
      {
        switchPlay();
        inTenSeconds();
        getPlayer().alpha=1;
        frontLayer.getChildByName("bar").alpha=1;
      }
    }

    public function switchPlay():void
    {
      canControl = !canControl;

    }

    public function ticker():void
    { 
      wallLayer.getChildByName("timeDot").alpha = 0.5;
      LoomTween.to(wallLayer.getChildByName("timeDot"),0.5,{"alpha":0});
      theTimer.schedule(1000,this,ticker);
    }

    public function makeGrid():void
    {
      for(var i:int = 0; i<28; i++)
        grid.pushSingle(new <int>[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }

    
  }
}
